const app = require("../index");
const reviewModel = require("../app/models/review.model");
const courseModel = require("../app/models/course.model");
const chai = require("chai");
const chaiHttp = require("chai-http");
const should = chai.should();

chai.use(chaiHttp);

describe("Test CRUD Review Restful API", () => {
    describe("/POST/ - Create", () => {
        after(async () => {
            // Xoá kết quả đầu tiên tìm thấy và sắp xếp theo giá trị insert cuối cùng trước
            await reviewModel.findOneAndDelete({}, { sort: { _id: -1 } });
        });

        it("should create a new review", async () => {
            const currentCourse = await courseModel.findOne().lean();

            const res = await chai.request(app).post("/api/v1/reviews/").send({
                stars: 4,
                note: "note",
                courseId: currentCourse._id,
            });

            res.should.have.status(201);
            // Kiểm tra dữ liệu được tạo
            res.body.should.be.a("object");
            res.body.should.have.property("note").equal("note");
        });
    });

    describe("/GET/ - Get all", () => {
        it("Returns all reviews must be array", (done) => {
            chai.request(app)
                .get("/api/v1/reviews")
                .end((err, res) => {
                    res.status.should.eql(200);
                    res.body.data.should.be.a("array");
                    done();
                });
        });
    });

    describe("/GET/:id - Get one", () => {
        after(async () => {
            // Xoá kết quả đầu tiên tìm thấy và sắp xếp theo giá trị insert cuối cùng trước
            await reviewModel.findOneAndDelete({}, { sort: { _id: -1 } });
        });

        it("should retrieve a review by its ID", async () => {
            const review = await reviewModel.create({
                stars: 4,
                note: "note",
            });

            const res = await chai
                .request(app)
                .get("/api/v1/reviews/" + review._id);

            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have.property("data");
            res.body.data.should.have
                .property("_id")
                .eql(review._id.toString());
        });
    });

    describe("/PUT/:id - Update one", () => {
        after(async () => {
            // Xoá kết quả đầu tiên tìm thấy và sắp xếp theo giá trị insert cuối cùng trước
            await reviewModel.findOneAndDelete({}, { sort: { _id: -1 } });
        });

        it("should update a course by its ID", async () => {
            const updateData = await reviewModel.create({
                stars: 4,
                note: "note",
            });

            const res = await chai
                .request(app)
                .put("/api/v1/reviews/" + updateData._id)
                .send({
                    stars: 4,
                    note: "updated note",
                });
            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have.property("data");
        });
    });

    describe("/DELETE/:id - Update one", () => {
        it("should update a course by its ID", async () => {
            let review = await reviewModel.create({
                stars: 4,
                note: "note",
            });

            const res = await chai
                .request(app)
                .delete("/api/v1/reviews/" + review._id);

            res.should.have.status(200);
            res.body.should.be.a("object");
        });
    });
});
