const app = require("../index");
const courseModel = require("../app/models/course.model");
const chai = require("chai");
const chaiHttp = require("chai-http");
const should = chai.should();

chai.use(chaiHttp);

describe("Test CRUD Course Restful API", () => {
    before(async () => {
        await courseModel.deleteMany({});
    });

    describe("/POST/ - Create", () => {
        it("should create a new course", (done) => {
            const data = {
                reqTitle: "title",
                reqDescription: "reqDescription",
                reqStudent: 10,
            };

            chai.request(app)
                .post("/api/v1/courses")
                .send(data)
                .end((err, res) => {
                    res.should.have.status(201);
                    // Kiểm tra dữ liệu được tạo
                    res.body.data.should.have.property("title").equal("title");
                    res.body.data.should.have
                        .property("description")
                        .equal("reqDescription");
                    res.body.data.should.have.property("noStudent").equal(10);
                    done();
                });
        });
    });

    describe("/GET/ - Get all", () => {
        it("Returns all courses must be array", (done) => {
            chai.request(app)
                .get("/api/v1/courses")
                .end((err, res) => {
                    res.status.should.eql(200);
                    res.body.data.should.be.a("array");
                    done();
                });
        });
    });

    describe("/GET/:id - Get one", () => {
        it("should retrieve a course by its ID", async () => {
            let course = await courseModel.create({
                title: "Ten",
                description: "Mo ta",
                noStudent: 10,
            });

            const res = await chai
                .request(app)
                .get("/api/v1/courses/" + course._id);

            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have.property("data");

            const courseData = res.body.data;
            courseData.should.have.property("title").eql("Ten");
            courseData.should.have.property("description").eql("Mo ta");
            courseData.should.have.property("noStudent").eql(10);
            courseData.should.have.property("_id").eql(course._id.toString());
        });
    });

    describe("/PUT/:id - Update one", () => {
        it("should update a course by its ID", async () => {
            let course = await courseModel.create({
                title: "Ten 1",
                description: "Mo ta",
                noStudent: 10,
            });

            const res = await chai
                .request(app)
                .put("/api/v1/courses/" + course._id)
                .send({
                    reqDescription: "Mo ta 1",
                    reqStudent: 11,
                });
            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have.property("data");

            const courseData = res.body.data;
            courseData.should.have.property("description").eql("Mo ta 1");
            courseData.should.have.property("noStudent").eql(11);
            courseData.should.have.property("_id").eql(course._id.toString());
        });
    });

    describe("/DELETE/:id - Update one", () => {
        it("should update a course by its ID", async () => {
            let course = await courseModel.create({
                title: "Ten 2",
                description: "Mo ta",
                noStudent: 10,
            });

            const res = await chai
                .request(app)
                .delete("/api/v1/courses/" + course._id);

            res.should.have.status(200);
            res.body.should.be.a("object");
        });
    });
});
